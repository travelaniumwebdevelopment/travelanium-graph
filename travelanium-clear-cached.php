<?php

use Travelanium\TravelaniumGraph;
require_once './travelanium.php';

/**
 * 
 * บังคับเคลียร์แคชทันที
 */
$number_of_cached_files = TravelaniumGraph::clear_all_cached(0, TRAVELANIUM_CACHE_PATH);

echo "$number_of_cached_files cached files deleted";