<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ajax Travelanium</title>
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-alpha1/css/bootstrap.min.css'/>
</head>
<body>
  <div class="container">
    <div>
      <div>Rateplan Test:</div>
      <span data-rateplan="PRO_40890"></span>

      <div>RoomType Test:</div>
      <span data-roomtype="RM_4896"></span>
    </div>

    <div style="height:100px"></div>
    <div id="app">
      <template>
        <div class="py-4" v-if="!loading">
          <article class="mb-4" v-for="item in items.data.rateplans" :data-id="item.code">
            <h1 class="h4">
              <a :href="item.infoURL" target="_blank">{{item.name}}</a> <span class="badge bg-info rounded-pill">{{item.productType | promotionType}}</span>
            </h1>

            <div v-if="item.images.length" class="mb-2">
              <img v-for="image in item.images" :src="image.size.medium.src" :width="image.size.medium.width" :height="image.size.medium.height" :alt="image.alt" />
            </div>

            <p>{{item.startingRate.finalFullRate || item.startingRate.finalRate | numberFormat}} THB</p>
          </article>
        </div>
        <div class="text-center py-4" v-else>Loading...</div>
      </template>
    </div>
  </div>

  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.11/vue.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js'></script>
  <script>
    new Vue({
      el: '#app',
      data: {
        loading: true,
        items: [],
      },
      created() {
        this.getData();
      },
      methods: {
        getData(params) {
          var rates = $.ajax({
            type: 'POST',
            url: '../travelanium-ajax.php',
            data: {
              action: 'get_rateplans',
              params: {
                apiVersion: 1,
                propertyId: '250',
                lang: 'th',
                currency: 'THB',
                ...params
              }
            },
          });

          rates.done( (res, status) => {
            if ('success' === status) {
              this.items = res;
            }
          } );

          rates.always(() => {
            this.loading = false;
          })
        }
      },
      filters: {
        promotionType( type ) {
          switch(type.toLowerCase()) {
            case 'pro': return 'Promotion'; break;
            case 'pkg': return 'Package'; break;
            case 'rat': return 'Rate'; break;
          }
        },
        numberFormat( num ) {
          return numeral(num).format("0,0.00");
        }
      }
    });
  </script>

  <script src="./example/index.js"></script>
</body>
</html>