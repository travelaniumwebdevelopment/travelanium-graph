<?php
require '../travelanium.php';
use Travelanium\TravelaniumGraph;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Single Rate</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-alpha1/css/bootstrap.min.css'/>
    <style>
        .page {
            width: 100%;
            max-width: 1500px;
            margin: auto;
        }
    </style>
</head>
<body>
    <?php
    $travelanium = new TravelaniumGraph();
    $travelanium->set_rateplan_params([
        'apiVersion' => 1,
        'propertyId' => '250',
        'lang'       => 'th',
        'currency'   => 'THB',
    ]);
    $travelanium->clear_all_cached();
    $rate = $travelanium->get_rateplan('PRO_34593');
    $rate = $rate->data->rateplan;
    ?>

    <div class="page">
        <div>Code: <?= $rate->code ?></div>
        <div>Name: <?= $rate->name ?></div>
        <div>Type: <?= $rate->productType ?></div>
        <div>Price: <?= $rate->startingRate->finalRate ?></div>
        <div>
            <div>Images:</div>
            <?php foreach( $rate->images as $image ) : ?>
            <?php
            $thumbnail = $image->size->thumbnail;
            $medium = $image->size->medium;
            $large = $image->size->large;
            $full = $image->size->full;
            ?>
            <img
                src="<?= $medium->src ?>"
                sizes="(max-width: 150px) 150px, (max-width: 300px) 300px, (max-width: 600px) 600px, 1200px"
                srcset="<?= $thumbnail->src ?> 150w, <?= $medium->src ?> 300w, <?= $large->src ?> 600w, <?= $full->src ?> 1200w"
                alt="<?= $image->alt ?>"
            />
            <?php endforeach ?>
        </div>
        <div>
            <a href="<?= $rate->infoURL ?>" target="_blank">Info Link</a><br />
            <a href="<?= $rate->bookingURL ?>" target="_blank">Book Link</a>
        </div>

        <pre style="margin-top:3rem;">
<?php print_r($rate); ?>
        </pre>
    </div>
</body>
</html>