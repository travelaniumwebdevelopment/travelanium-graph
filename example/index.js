(function($) {
  var rateplans = $.ajax({
    type: 'post',
    url: '../travelanium-ajax.php',
    data: {
      action: 'get_rateplans',
      params: {
        propertyId: 250,
        currency: 'THB',
        lang: 'th',
      }
    }
  });

  rateplans.then(function(res) {
    if (res.success) {
      var data = res.data.rateplans;

      $('[data-rateplan]').each(function() {
        var el = this;
        var code = el.dataset.rateplan;
        if (!code) return;

        var rate = data.filter(function(item) {
          return item.code === code;
        })[0];

        var price = rate.startingRatePerNightPerPerson.finalFullRate;
        price = numeral(price);
        $(el).text( price.format('0,0') );
      });
    }
  });

  var roomtypes = $.ajax({
    type: 'post',
    url: '../travelanium-ajax.php',
    data: {
      action: 'get_roomtypes',
      params: {
        propertyId: 250,
        currency: 'THB',
        lang: 'th',
      }
    }
  });

  roomtypes.then(function(res) {
    if (!res.success) return;

    var data = res.data.roomtypes;

    $('[data-roomtype]').each(function() {
      var el = this;
      var code = el.dataset.roomtype;
      if (!code) return;

      var rate = data.filter(function(item) {
        return item.code === code;
      })[0]

      var price = rate.startingRatePerNightPerPerson.finalFullRate;
      price = numeral(price);
      $(el).text( price.format('0,0') );
    });
  })
})(jQuery);