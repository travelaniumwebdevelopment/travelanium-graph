<?php
use Travelanium\TravelaniumGraph;
require_once '../travelanium.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Get Room Types - Travelanium</title>
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-alpha1/css/bootstrap.min.css'/>
</head>
<body>
  <?php
  $travelanium = new TravelaniumGraph( [
    'api_key' => '7f299529ac1200023dadedcf5b0e9c70',
  ] );

  $rates = $travelanium->get_roomtypes( [
    'apiVersion' => 1,
    'propertyId' => '250',
    'lang' => 'th',
    'currency' => 'THB',
  ] );

  $rates = $rates->data->roomtypes;
  ?>

  <div class="container py-4">
    <h1 class="mb-5">Rateplans</h1>

    <?php if ($rates) : ?>
    <?php foreach($rates as $item) : ?>
    <article>
      <h1 class="h3"><a href="<?= $item->bookingURL ?>"><?= $item->name ?></a></h1>
  
      <?php if (count($item->images)) : ?>
      <div>
        <img
          src="<?= $item->images[0]->size->medium->src ?>"
          width="<?= $item->images[0]->size->medium->width ?>"
          height="<?= $item->images[0]->size->medium->height ?>"
          alt="<?= $item->images[0]->alt ?>"
        />
      </div>

      <div class="mt-3">Price: THB <?= number_format( $item->startingRate->fullRate ) ?></div>
      <?php endif; ?>
    </article>
    <?php endforeach ?>
    <?php else : ?>
    <p>No rates found.</p>
    <?php endif ?>

    <div class="mt-5">Footer</div>
  </div>
</body>
</html>