# Travelanium Graph

ตัวช่วยในการเข้าถึงข้อมูลของเรตผ่าน Graph API


---

## สารบัญ

- [Travelanium Graph](#travelanium-graph)
  - [สารบัญ](#สารบัญ)
  - [Getting Started](#getting-started)
  - [SETUP: configs.php](#setup-configsphp)
  - [SETUP TravelaniumGraph class](#setup-travelaniumgraph-class)
    - [TravelaniumGraph( $options )](#travelaniumgraph-options-)
      - [Parameters](#parameters)
      - [Example](#example)
  - [Methods](#methods)
    - [get_rateplans( $params )](#get_rateplans-params-)
      - [Parameters](#parameters-1)
      - [Example](#example-1)
    - [get_rateplan( $rate_id, $params )](#get_rateplan-rate_id-params-)
      - [Parameters](#parameters-2)
      - [Example](#example-2)
    - [set_rateplan_params( $params )](#set_rateplan_params-params-)
      - [Parameters](#parameters-3)
      - [Example](#example-3)
    - [clear_all_cached( $limit, $path )](#clear_all_cached-limit-path-)
      - [Parameters](#parameters-4)
      - [Example](#example-4)
  - [ตัวอย่าง Results](#ตัวอย่าง-results)
    - [get_rateplans](#get_rateplans)
    - [get_rateplan](#get_rateplan)
    - [get_roomtypes](#get_roomtypes)
    - [get_roomtype](#get_roomtype)


---


## Getting Started

1. ดาวน์โหลด [travelanium-graph-0.4.6.zip](https://bitbucket.org/travelaniumwebdevelopment/travelanium-graph/downloads/travelanium-graph-0.4.6.zip)
2. extract ไฟล์ที่ดาวน์โหลด แล้วนำไดเร็กทอรี travelanium-graph ใส่ไว้ในเว็บไซต์ที่ใดก็ได้ที่สะดวกในการเรียกใช้งาน
3. ให้แก้ไขไฟล์ travelanium-graph/configs.php โดยนำ api_key ใส่ในตัวแปร `TRAVELANIUM_API_KEY`
4. สร้างฟังก์ชันสำหรับใช้งานได้ทันที ด้วยวิธีต่อไปนี้



## SETUP: configs.php

**configs.php** เป็นไฟล์ที่รวมตัวแปรสำคัญต่าง ๆ เอาไว้ โดยส่วนใหญ่แล้วสามารถทำงานได้โดยค่าเริ่มต้น มีเพียงเฉพาะตัวแปร `TRAVELANIUM_API_KEY` เท่านั้น ที่จำเป็นต้องใส่



## SETUP TravelaniumGraph class

### TravelaniumGraph( $options )

สร้าง instance สำหรับการใช้ Travelanium Graph

#### Parameters

`$options` (array)

โดยปกติ $options จะถูกดึงมาจาก configs.php อยู่แล้ว หากไม่จำเป็นต้องกำหนดค่าอะไรที่แตกต่างไป เราก็สามารถสร้าง instance โดยไม่ต้องกำหนด $options ใด ๆ เลยก็ได้เช่นกัน

| Parameter             | Description |
|-----------------------|-------------|
| api_key               | ใช้กำหนด api ที่แตกต่างไปจาก configs.php ได้ default: `TRAVELANIUM_API_KEY` |
| cache_path            | ใช้กำหนดตำแหน่งแคชไฟล์ default: `TRAVELANIUM_CACHE_PATH` |
| token_refresh_timer   | ใช้กำหนดเวลาในการขอ token ใหม่ default: `TOKEN_REFRESH_TIMER` (12 ชั่วโมง) |
| rateplans_cache_timer | ใช้กำหนดอายุของแคชสำหรับข้อมูล rateplans default: `QUERY_CACHE_TIMER` (10 นาที) |
| rateplan_cache_timer  | ใช้กำหนดอายุของแคชสำหรับข้อมูล rateplan default: `QUERY_CACHE_TIMER` (10 นาที) |

#### Example

```php
<?php
use Travelanium\TravelaniumGraph;
require 'path/to/travelanium.php';

$graph = new TravelaniumGraph([
  'api_key' => '__your_api_key__',
  'cache_path' => 'cache/',
  'token_refresh_timer' => 43200,
  'rateplans_cache_timer' => 600,
  'rateplan_cache_timer' => 600,
]);
?>
```



หลังจากนั้นเราสามารถเรียกใช้งาน [Methods](#methods) ต่าง ๆ ได้ทันที



## Methods



### get_rateplans( $params )

ใช้เพื่อเรียกเรต**ทั้งหมด**ที่ Active ในระบบอยู่

#### Parameters

`$params` (array)

| Parameter  | Description |
|------------|-------------|
| apiVersion | (default: 1) |
| propertyId | (default: *null*) (Required) |
| lang       | (default: th) กำหนดภาษาของข้อมูลเรตทีได้รับ options: th, en, zh, ko, ru |
| currency   | (default: 'THB') กำหนดสกุลเพื่อให้ได้ค่าเงินตรงกับที่ต้องการ options: THB, USD, EUR, CNY, AUD, GBP, HKD, INR, JPY, MYR, RUB, SGD, KRW, CHF |
| accessCode | (default: '') ใส่ในกรณีที่ต้องการให้แสดงข้อมูลตาม accesscode ที่ระบุ |

#### Example

```php
<?php
$rates = $graph->get_rateplans( [
  'apiVersion' => 1,
  'propertyId' => '250',
  'lang' => 'th',
  'currency' => 'THB',
  'accessCode' => '',
] );

print_r( $rates->data->rateplans );
?>
```



### get_rateplan( $rate_id, $params )

ใช้เพื่อเรียกเรตตาม ID ที่ระบุใน $rate_id

#### Parameters

`$rate_id` (string)

ID ของเรตที่ต้องการข้อมูล

`$params` (array)

| Parameter  | Description |
|------------|-------------|
| apiVersion | (default: 1) |
| propertyId | (default: *null*) (Required) |
| lang       | (default: th) กำหนดภาษาของข้อมูลเรตทีได้รับ options: th, en, zh, ko, ru |
| currency   | (default: 'THB') กำหนดสกุลเพื่อให้ได้ค่าเงินตรงกับที่ต้องการ options: THB, USD, EUR, CNY, AUD, GBP, HKD, INR, JPY, MYR, RUB, SGD, KRW, CHF |
| accessCode | (default: '') ใส่ในกรณีที่ต้องการให้แสดงข้อมูลตาม accesscode ที่ระบุ |

#### Example

```php
<?php
$rate = $graph->get_rateplan( 'XXX_YYYY', [
  'apiVersion' => 1,
  'propertyId' => '250',
  'lang' => 'th',
  'currency' => 'THB',
  'accessCode' => '',
] );

print_r( $rate->data->rateplan );
?>
```



### set_rateplan_params( $params )

ใช้สำหรับกำหนด default parameter สำหรับการเรียก get_rateplans หรือ get_rateplan ได้

#### Parameters

`$params` (array)

| Parameter  | Description |
|------------|-------------|
| apiVersion | (default: 1) |
| propertyId | (default: *null*) (Required) |
| lang       | (default: th) กำหนดภาษาของข้อมูลเรตทีได้รับ options: th, en, zh, ko, ru |
| currency   | (default: 'THB') กำหนดสกุลเพื่อให้ได้ค่าเงินตรงกับที่ต้องการ options: THB, USD, EUR, CNY, AUD, GBP, HKD, INR, JPY, MYR, RUB, SGD, KRW, CHF |
| accessCode | (default: '') ใส่ในกรณีที่ต้องการให้แสดงข้อมูลตาม accesscode ที่ระบุ |

#### Example
```php
<?php
use Travelanium\TravelaniumGraph;
require 'path/to/travelanium.php';

$graph = new TravelaniumGraph();

// กำหนด parameters เริ่มต้น
$graph->set_rateplan_params( [
  'apiVersion' => 1,
  'propertyId' => '9999',
  'lang' => 'en',
  'currency' => 'JPY',
  'accessCode' => 'special_deals',
] );

// ดึงข้อมูลเรตที่ active ทั้งหมด โดยไม่ต้องกำหนดอะไรเพิ่มเติม
$deals_rates = $graph->get_rateplans();
print_r( $deals_rates->data->rateplans; );

// ดึงข้อมูลเรตที่ active ตาม $rate_id ที่กำหนด โดยไม่ต้องกำหนดอะไรเพิ่มเติม
$single_deal = $graph->get_rateplan( 'XXX_YYYY' );
print_r( $single_deal->data->rateplan );
?>
```



### clear_all_cached( $limit, $path )

ใช้สำหรับกำหนดเวลาลบแคชไฟล์ทั้งหมดที่สะสมไว้ในระบบ

#### Parameters

`$limit` (string|int)

กำหนดขอบเขตของระยะเวลาที่แคชไฟล์ที่ถูกสร้างไว้จะถูกลบออก หากมีอายุเกินที่กำหนด เช่น WEEKLY หมายถึงให้ลบไฟล์แคชทุกไฟล์ที่มีอายุเกินหนึ่งสัปดาห์ options: 'DAILY', 'WEEKLY' (default), 'MONTHLY', 'YEARLY', ตัวเลขเป็นวินาที

`$path` (string)

พาธของแคชไดเร็กทอรี่ default: *ตามที่กำหนดไว้ใน configs.php (cache/)*

#### Example
```php
<?php

// ลบไฟล์แคชที่อายุเกิน 1 สัปดาห์
$graph->clear_all_cached( 'WEEKLY' );

// ลบไฟล์แคชที่อายุเกิน 3600 วินาที (1 ชั่วโมง)
$graph->clear_all_cached( 3600 );

// ลบไฟล์แคชแบบสั่งจากคำสั่งภายนอกโดยตรงได้ (จำเป็นต้องกำหนด $path ด้วย)
TravelaniumGraph::clear_all_cached( 'DAILY', 'cache/' );

// ลบไฟล์แคชแบบสั่งจากคำสั่งภายนอกโดยตรง ทุก 0 วินาที (บังคำลบแคชทั้งหมดในทันที) (จำเป็นต้องกำหนด $path ด้วย)
TravelaniumGraph::clear_all_cached( 0, 'cache/' );

?>
```



## ตัวอย่าง Results

### get_rateplans

``` json
{
  "data": {
    "rateplans": [
      {
        "code": "PRO_15558",
        "name": "Long Stay Package- Stay Longer, Save More! (Breakfast)",
        "productType": "PRO",
        "infoURL": "https://reservation.thavornpalmbeach.com/hotelpage/rates/?propertyId=250&onlineId=8&pid=MDcxNTU1OA%3D%3D&lang=en&currency=THB",
        "bookingURL": "https://reservation.thavornpalmbeach.com/propertyibe2/rates?propertyId=250&onlineId=8&lang=en&currency=THB&pid=MDcxNTU1OA%3D%3D",
        "bookingPeriods": [],
        "images": [
          {
            "alt": "Long Stay Package- Stay Longer, Save More! (Breakfast)",
            "size": {
              "thumbnail": {
                "src": "https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=250&group=23&width=150&height=100&imageid=5837&type=jpg",
                "width": 150,
                "height": 100
              },
              "medium": {
                "src": "https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=250&group=23&width=300&height=200&imageid=5837&type=jpg",
                "width": 300,
                "height": 200
              },
              "large": {
                "src": "https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=250&group=23&width=600&height=400&imageid=5837&type=jpg",
                "width": 600,
                "height": 400
              },
              "full": {
                "src": "https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=250&group=24&imageid=5837&type=jpg",
                "width": 450,
                "height": 300
              }
            }
          }
        ],
        "startingRate": {
          "rate": 8216.43,
          "finalRate": 9752.9,
          "fullRate": null,
          "finalFullRate": null
        },
        "startingRatePerNight": {
          "rate": 1643.29,
          "finalRate": 1950.58,
          "fullRate": null,
          "finalFullRate": null
        },
        "startingRatePerNightPerPerson": {
          "rate": 821.64,
          "finalRate": 975.29,
          "fullRate": null,
          "finalFullRate": null
        }
      },
      {...}
      {...}
      {...}
    ]
  }
}
```

### get_rateplan

``` json
{
  "data": {
    "rateplan": {
      "code": "PRO_15558",
      "name": "Long Stay Package- Stay Longer, Save More! (อาหารเช้าฟรี)",
      "productType": "PRO",
      "infoURL": "https://reservation.thavornpalmbeach.com/hotelpage/rates/?propertyId=250&onlineId=8&pid=MDcxNTU1OA%3D%3D&lang=th&currency=THB",
      "bookingURL": "https://reservation.thavornpalmbeach.com/propertyibe2/rates?propertyId=250&onlineId=8&lang=th&currency=THB&pid=MDcxNTU1OA%3D%3D",
      "bookingPeriods": [],
      "images": [
        {
          "alt": "Long Stay Package- Stay Longer, Save More! (อาหารเช้าฟรี)",
          "size": {
            "thumbnail": {
              "src": "https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=250&group=23&width=150&height=100&imageid=5837&type=jpg",
              "width": 150,
              "height": 100
            },
            "medium": {
              "src": "https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=250&group=23&width=300&height=200&imageid=5837&type=jpg",
              "width": 300,
              "height": 200
            },
            "large": {
              "src": "https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=250&group=23&width=600&height=400&imageid=5837&type=jpg",
              "width": 600,
              "height": 400
            },
            "full": {
              "src": "https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=250&group=24&imageid=5837&type=jpg",
              "width": 450,
              "height": 300
            }
          }
        }
      ],
      "startingRate": {
        "rate": 8216.43,
        "finalRate": 9752.9,
        "fullRate": null,
        "finalFullRate": null
      },
      "startingRatePerNight": {
        "rate": 1643.29,
        "finalRate": 1950.58,
        "fullRate": null,
        "finalFullRate": null
      },
      "startingRatePerNightPerPerson": {
        "rate": 821.64,
        "finalRate": 975.29,
        "fullRate": null,
        "finalFullRate": null
      }
    }
  }
}
```

### get_roomtypes

``` json
{
	"data": {
		"roomtypes": [
      {
				"code": "RM_1336",
				"name": "Deluxe Terrace",
				"startingRate": {
					"rate": 1853.42,
					"finalRate": 2200.01,
					"fullRate": 3634.15,
					"finalFullRate": 4313.74
				},
				"startingRatePerNight": {
					"rate": 1853.42,
					"finalRate": 2200.01,
					"fullRate": 3634.15,
					"finalFullRate": 4313.74
				},
				"startingRatePerNightPerPerson": {
					"rate": 926.71,
					"finalRate": 1100.0,
					"fullRate": 1817.08,
					"finalFullRate": 2156.87
				},
				"bookingURL": "https://reservation.thavornpalmbeach.com/propertyibe2/rates?propertyId=250&onlineId=7&lang=en&currency=THB&rid=05681da6559caac5b15417587296d193",
				"images": [
          {
            "alt": "Deluxe Terrace",
            "size": {
              "thumbnail": {
                "src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=3&width=150&height=100&imageid=51176&type=jpg",
                "width": 150,
                "height": 100
              },
              "medium": {
                "src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=3&width=450&height=300&imageid=51176&type=jpg",
                "width": 300,
                "height": 200
              },
              "large": null,
              "full": {
                "src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=5&imageid=51176&type=jpg",
                "width": 1000,
                "height": 667
              }
            }
				  }
        ]
			},
			{
				"code": "RM_4896",
				"name": "Deluxe Convenient Central Garden Access",
				"startingRate": {
					"rate": 2105.94,
					"finalRate": 2499.75,
					"fullRate": 4129.3,
					"finalFullRate": 4901.48
				},
				"startingRatePerNight": {
					"rate": 2105.94,
					"finalRate": 2499.75,
					"fullRate": 4129.3,
					"finalFullRate": 4901.48
				},
				"startingRatePerNightPerPerson": {
					"rate": 1052.97,
					"finalRate": 1249.88,
					"fullRate": 2064.65,
					"finalFullRate": 2450.74
				},
				"bookingURL": "https://reservation.thavornpalmbeach.com/propertyibe2/rates?propertyId=250&onlineId=7&lang=en&currency=THB&rid=1b0de47b669be954cb0031cd35fdc88a",
				"images": [
          {
						"alt": "Deluxe Convenient Central Garden Access",
						"size": {
							"thumbnail": {
								"src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=3&width=150&height=100&imageid=51187&type=jpg",
								"width": 150,
								"height": 100
							},
							"medium": {
								"src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=3&width=450&height=300&imageid=51187&type=jpg",
								"width": 300,
								"height": 200
							},
							"large": null,
							"full": {
								"src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=5&imageid=51187&type=jpg",
								"width": 1000,
								"height": 667
							}
						}
					},
					{
						"alt": "Deluxe Convenient Central Garden Access",
						"size": {
							"thumbnail": {
								"src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=3&width=150&height=100&imageid=51189&type=jpg",
								"width": 150,
								"height": 100
							},
							"medium": {
								"src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=3&width=450&height=300&imageid=51189&type=jpg",
								"width": 300,
								"height": 200
							},
							"large": null,
							"full": {
								"src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=5&imageid=51189&type=jpg",
								"width": 1000,
								"height": 667
							}
						}
					}
				]
			}
		]
	}
}
```

### get_roomtype

``` json
{
	"data": {
		"roomtype": {
			"code": "RM_4896",
			"name": "Deluxe Convenient Central Garden Access",
			"startingRate": {
				"rate": 2105.94,
				"finalRate": 2499.75,
				"fullRate": 4129.3,
				"finalFullRate": 4901.48
			},
			"startingRatePerNight": {
				"rate": 2105.94,
				"finalRate": 2499.75,
				"fullRate": 4129.3,
				"finalFullRate": 4901.48
			},
			"startingRatePerNightPerPerson": {
				"rate": 1052.97,
				"finalRate": 1249.88,
				"fullRate": 2064.65,
				"finalFullRate": 2450.74
			},
			"bookingURL": "https://reservation.thavornpalmbeach.com/propertyibe2/rates?propertyId=250&onlineId=7&lang=en&currency=THB&rid=1b0de47b669be954cb0031cd35fdc88a",
			"images": [
        {
					"alt": "Deluxe Convenient Central Garden Access",
					"size": {
						"thumbnail": {
							"src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=3&width=150&height=100&imageid=51187&type=jpg",
							"width": 150,
							"height": 100
						},
						"medium": {
							"src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=3&width=450&height=300&imageid=51187&type=jpg",
							"width": 300,
							"height": 200
						},
						"large": null,
						"full": {
							"src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=5&imageid=51187&type=jpg",
							"width": 1000,
							"height": 667
						}
					}
				},
				{
					"alt": "Deluxe Convenient Central Garden Access",
					"size": {
						"thumbnail": {
							"src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=3&width=150&height=100&imageid=51189&type=jpg",
							"width": 150,
							"height": 100
						},
						"medium": {
							"src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=3&width=450&height=300&imageid=51189&type=jpg",
							"width": 300,
							"height": 200
						},
						"large": null,
						"full": {
							"src": "https://images.travelanium.net/crs-file-manager/images/room?propertyid=250&group=5&imageid=51189&type=jpg",
							"width": 1000,
							"height": 667
						}
					}
				}
			]
		}
	}
}
```