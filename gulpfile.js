var {task, src, dest, parallel, series, watch} = require('gulp');
var zip = require('gulp-zip');
var clean = require('gulp-clean');
var rename = require('gulp-rename');
var version = require('./package.json')['version'];

task('copy', () => {
  return src([
    './classes/**/*',
    './schema/**/*',
    './vendor/**/*',
    'configs.php',
    'index.php',
    'travelanium.php',
    'travelanium-ajax.php',
    'travelanium-clear-cached.php',
  ], {
    base: '.'
  })
    .pipe(dest(`./downloads/travelanium-graph-${version}`))
});

task('zip', () => {
  return src(`./downloads/travelanium-graph-${version}/**`)
    .pipe(rename(path => {
      path.dirname = `travelanium-graph-${version}/${path.dirname}`;
    }))
    .pipe(zip(`travelanium-graph-${version}.zip`))
    .pipe(dest('./downloads'))
});

task('clean', () => {
  return src(`./downloads/travelanium-graph-${version}`, {read: false})
    .pipe(clean())
});

task('packing', series('copy', 'zip', 'clean'));