<?php
/**
 * Travelanium Graph class
 * API Documentation: https://bitbucket.org/travelaniumwebdevelopment/travelanium-graph
 *
 * @author Travelanium
 * @since 2020-10-01
 * @version 0.4.6
 */
namespace Travelanium;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Cache;

require 'configs.php';
require 'vendor/autoload.php';
require 'classes/cache.class.php';

class TravelaniumGraph
{
    const BASE_AUTH_URL      = 'https://graph.travelanium.net/api/auth';
    const BASE_RATEPLANS_URL = 'https://graph.travelanium.net/api/v1/property';
    const UNIX_PERM          = 755;
    
    protected $api_key;
    protected $cache_path;
    protected $token_refresh_timer;
    protected $rateplans_cache_timer;
    protected $rateplan_cache_timer;
    protected $roomtypes_cache_timer;
    protected $roomtype_cache_timer;

    public $graphql_schema_rateplan;
    public $graphql_schema_rateplans;
    public $rateplan_params;

    public $graphql_schema_roomtype;
    public $graphql_schema_roomtypes;
    public $roomtype_params;
    
    public function __construct( $args = [] )
    {
        $configs = array_merge( [
            'api_key'               => TRAVELANIUM_API_KEY,
            'cache_path'            => TRAVELANIUM_CACHE_PATH,
            'token_refresh_timer'   => TOKEN_REFRESH_TIMER,
            'rateplans_cache_timer' => QUERY_CACHE_TIMER,
            'rateplan_cache_timer'  => QUERY_CACHE_TIMER,
            'roomtypes_cache_timer' => QUERY_CACHE_TIMER,
            'roomtype_cache_timer'  => QUERY_CACHE_TIMER,
        ], $args );

        if (isset($configs['api_key'])) {
            $this->api_key = $configs['api_key'];
        }

        if (isset($configs['cache_path'])) {
            $this->cache_path = $configs['cache_path'];
        }

        if (isset($configs['token_refresh_timer'])) {
            $this->token_refresh_timer = $configs['token_refresh_timer'];
        }

        if (isset($configs['rateplans_cache_timer'])) {
            $this->rateplans_cache_timer = $configs['rateplans_cache_timer'];
        }

        if (isset($configs['rateplan_cache_timer'])) {
            $this->rateplan_cache_timer = $configs['rateplan_cache_timer'];
        }

        if (isset($configs['roomtypes_cache_timer'])) {
            $this->roomtypes_cache_timer = $configs['roomtypes_cache_timer'];
        }

        if (isset($configs['roomtype_cache_timer'])) {
            $this->roomtype_cache_timer = $configs['roomtype_cache_timer'];
        }

        $this->rateplan_params = [
            'apiVersion'   => (Int) 1,
            'propertyId'   => (String) NULL,
            'lang'         => (String) 'en',
            'currency'     => (String) 'THB',
            'ratePlanCode' => (String) '',
        ];

        $this->roomtype_params = [
            'apiVersion'   => (Int) 1,
            'propertyId'   => (String) NULL,
            'lang'         => (String) 'en',
            'currency'     => (String) 'THB',
            'roomtypeCode' => (String) '',
        ];

        $this->graphql_schema_rateplan  = file_get_contents(__DIR__ . '/schema/get_rateplan_inline.gql');
        $this->graphql_schema_rateplans = file_get_contents(__DIR__ . '/schema/get_rateplans_inline.gql');
        $this->graphql_schema_roomtype = file_get_contents(__DIR__ . '/schema/get_roomtype_inline.gql');
        $this->graphql_schema_roomtypes = file_get_contents(__DIR__ . '/schema/get_roomtypes_inline.gql');
    }
        
    /**
     * Method set_rateplan_params
     * เซ็ต params เริ่มต้นสำหรับการดึงข้อมูล Rate Plans ผ่าน method: get_rateplans() และ get_rateplan()
     *
     * @param Object $add_params
     *
     * @return void
     */
    public function set_rateplan_params( $add_params )
    {
        $params = array_merge( $this->rateplan_params, $add_params );
        $this->rateplan_params = $params;
    }

    public function set_roomtype_params( $add_params )
    {
        $params = array_merge( $this->roomtype_params, $add_params );
        $this->roomtype_params = $params;
    }
    
    /**
     * Method set_graphql_schema_rateplan
     *
     * @param Object $schema
     *
     * @return void
     */
    public function set_graphql_schema_rateplan( $schema )
    {
        $this->graphql_schema_rateplan = $schema;
    }
    
    /**
     * Method set_graphql_schema_rateplans
     *
     * @param $schema $schema [explicite description]
     *
     * @return void
     */
    public function set_graphql_schema_rateplans( $schema )
    {
        $this->graphql_schema_rateplans = $schema;
    }

    public function fetch_token()
    {
        $tokenCached = new Cache();
        $tokenCached->setCache('token');
        $tokenCached->setCachePath( $this->cache_path );
        
        if ( $tokenCached->isCached('token') ) {
            $data = $tokenCached->retrieve('token');
        } else {
            try {
                $client   = new Client();
                $response = $client->request('POST', Self::BASE_AUTH_URL, [
                    'headers' => [
                        'Content-Tmype'  => 'application/x-www-form-urlencoded',
                        'X-API-KEY'     => $this->api_key,
                    ],
                ]);
                $data = $response->getHeader('Authorization')[0];
                $tokenCached->store('token', $data, $this->token_refresh_timer);
            } catch (BadResponseException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                $data = [
                    'error' => $responseBodyAsString,
                ];
            }
        }

        return $data;
    }

    public function decode_token( $token_raw )
    {
        $token = str_replace('Bearer ', '', $token_raw);
        list( $header, $playload, $verify ) = explode('.', $token);
        
        return [
            'token'    => $token,
            'header'   => base64_decode($header),
            'playload' => base64_decode($playload),
            'verify'   => $verify,
        ];
    }

    public function get_token()
    {
        $token = $this->fetch_token();
        $token = $this->decode_token($token);
        return $token;
    }

    public function get_token_header()
    {
        $token = $this->get_token();
        return json_decode($token['header']);
    }
    
    public function get_token_playload()
    {
        $token = $this->get_token();
        return json_decode($token['playload']);
    }
    
    public function get_token_verify()
    {
        $token = $this->get_token();
        return $token['verify'];
    }

    public function hash_string( $args )
    {
        return md5($args);
    }

    /**
     * Clear all cached by time 
     * 
     * @param INT|STRING $limit เวลาในระดับวินาที โดยฟังก์ชั่นจะทำการเคลียร์ไฟล์ที่มีอายุเกินกว่ากำหนด ค่าเริ่มต้นคือ 604800 (7 วัน)
     * 
     * DAILY, WEEKLY, MONTHLY, YEARLY
     * 
     * @return INT จำนวนไฟล์แคชที่ถูกลบ
     */
    public function clear_all_cached( $limit = 'WEEKLY', $path = null )
    {
        if (!$path) {
            $path = $this->cache_path;
        }

        if ( is_numeric( $limit ) ) {
            $seconds = $limit;
        } else {
            switch( $limit ) {
                case 'DAILY': $seconds = 86400; break;
                case 'WEEKLY': $seconds = 604800; break;
                case 'MONTHLY': $seconds = 2592000; break;
                case 'YEARLY': $seconds = 31536000; break;
                default: $seconds = $limit;
            }
        }

        $files = glob( $path . '*.cache');
        $del = [];
        foreach( $files as $file ) {
            if ( time() - filemtime($file) > $seconds ) {
                $del[] = unlink($file);
            }
        }

        return count($del);
    }

    /**
     * Check if object has errors
     * 
     * @param MIXED $response ข้อมูลตอบจากการ REQUEST
     */
    public function is_error( $response )
    {
        if ( array_key_exists('errors', $response) ) {
            return $response->errors[0]->message;
        }
        return false;
    }

    /**
     * Get GraphQL query string
     * 
     * @param STRING $schema สกีมามาสเตอร์เทมเพลต
     * @param OBJECT $params พาราเมเตอร์ต่าง ๆ ที่ใช้ในการดึงเรต
     *
     * @return STRING
     */
    private function get_graph_query( $schema, $params )
    {
        $str_search  = [];
        $str_replace = [];
        $set_types   = [
            'apiVersion'   => 'Int',
            'propertyId'   => 'String',
            'currency'     => 'String',
            'lang'         => 'String',
            'ratePlanCode' => 'String',
            'roomtypeCode' => 'String',
        ];

        foreach( $params as $key => $val ) {
            if (!in_array($key, array_keys($set_types))) {
                continue;
            }

            switch( strtoupper($set_types[$key]) ) {
                case 'INT':
                    $val = $val;
                    break;
                case 'BOOL':
                case 'BOOLEAN':
                    $val = !!$val;
                    break;
                case 'STRING':
                default:
                    $val = "\"$val\"";
                    break;
            }

            $str_search[]  = "%$key%";
            $str_replace[] = $val;
        }

        return json_encode(
            [
                'query' => str_replace($str_search, $str_replace, $schema),
            ]
        );
    }

    /**
     * 
     * Get all rate data
     * 
     * @param ARRAY $params Options สำหรับการดึงข้อมูลมา
     * 
     * @return OBJECT
     * 
     */
    public function get_rateplans( $params = [] )
    {
        $params          = array_merge($this->rateplan_params, $params);
        $graphql         = $this->get_graph_query( $this->graphql_schema_rateplans, $params );
        $queryHashed     = $this->hash_string($graphql);

        $rateplansCached = new Cache();
        $rateplansCached->setCache($queryHashed);
        $rateplansCached->setCachePath( $this->cache_path );

        if ( $rateplansCached->isCached($queryHashed) ) {
            $data = $rateplansCached->retrieve($queryHashed);
        } else {
            try {
                $client   = new Client();
                $response = $client->request('POST', Self::BASE_RATEPLANS_URL, [
                    "headers" => [
                        "Content-type"  => "application/json",
                        "Authorization" => $this->fetch_token(),
                    ],
                    "body"    => $graphql,
                ] );
                $data = json_decode($response->getBody());

                if ( $msg = $this->is_error( $data ) ) {
                    trigger_error( $msg, E_USER_WARNING );
                    return $msg;
                }

                $rateplansCached->store($queryHashed, $data, $this->rateplans_cache_timer);
            } catch (BadResponseException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                $data = [
                    'error' => $responseBodyAsString,
                    'data' => $response,
                ];
            }
        }

        return $data;
    }

    /**
     * 
     * Get single rate data
     * 
     * @param STRING $rate_id ID ของ rate ที่ต้องการดึงข้อมูลมา
     * @param ARRAY $params Options สำหรับการดึงข้อมูลมา
     * 
     * @return OBJECT
     * 
     */
    public function get_rateplan( $rate_id, $params = [] )
    {
        $params         = array_merge( $this->rateplan_params, ['ratePlanCode' => (String) $rate_id], $params );
        $graphql        = $this->get_graph_query( $this->graphql_schema_rateplan, $params );
        $queryHashed    = $this->hash_string($graphql);
        $rateplanCached = new Cache();

        $rateplanCached->setCache($queryHashed);
        $rateplanCached->setCachePath( $this->cache_path );

        if ( $rateplanCached->isCached($queryHashed) ) {
            $data = $rateplanCached->retrieve($queryHashed);
        } else {
            try {
                $client   = new Client();
                $response = $client->request('POST', Self::BASE_RATEPLANS_URL, [
                    "headers" => [
                        "Content-type"  => "application/json",
                        "Authorization" => $this->fetch_token(),
                    ],
                    "body"    => $graphql,
                ]);
                $data = json_decode($response->getBody());

                if ( $msg = $this->is_error( $data ) ) {
                    trigger_error( $msg, E_USER_WARNING );
                    return $msg;
                }

                $rateplanCached->store($queryHashed, $data, $this->rateplan_cache_timer);
            } catch (BadResponseException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                $data = [
                    'error' => $responseBodyAsString,
                    'data' => $response,
                ];
            }
        }

        return $data;
    }

    /**
     * Get rates by room type
     *
     * @param array $params
     * @return RoomsRate
     */
    public function get_roomtypes( array $params = [] )
    {
        $params          = array_merge($this->roomtype_params, $params);
        $graphql         = $this->get_graph_query( $this->graphql_schema_roomtypes, $params );
        $queryHashed     = $this->hash_string($graphql);

        $roomtypesCached = new Cache();
        $roomtypesCached->setCache($queryHashed);
        $roomtypesCached->setCachePath( $this->cache_path );

        if ( $roomtypesCached->isCached($queryHashed) ) {
            $data = $roomtypesCached->retrieve($queryHashed);
        } else {
            try {
                $client   = new Client();
                $response = $client->request('POST', Self::BASE_RATEPLANS_URL, [
                    "headers" => [
                        "Content-type"  => "application/json",
                        "Authorization" => $this->fetch_token(),
                    ],
                    "body"    => $graphql,
                ] );
                $data = json_decode($response->getBody());

                if ( $msg = $this->is_error( $data ) ) {
                    trigger_error( $msg, E_USER_WARNING );
                    return $msg;
                }

                $roomtypesCached->store($queryHashed, $data, $this->roomtypes_cache_timer);
            } catch (BadResponseException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                $data = [
                    'error' => $responseBodyAsString,
                    'data' => $response,
                ];
            }
        }

        return $data;
    }

    /**
     * Get single rate by room type
     *
     * @param string $room_id
     * @param array $params
     * @return SingleRoomRate
     */
    public function get_roomtype( string $room_id, array $params = [] )
    {
        $params         = array_merge( $this->roomtype_params, ['roomtypeCode' => (String) $room_id], $params );
        $graphql        = $this->get_graph_query( $this->graphql_schema_roomtype, $params );
        $queryHashed    = $this->hash_string($graphql);
        $roomtypeCached = new Cache();

        $roomtypeCached->setCache($queryHashed);
        $roomtypeCached->setCachePath( $this->cache_path );

        if ( $roomtypeCached->isCached($queryHashed) ) {
            $data = $roomtypeCached->retrieve($queryHashed);
        } else {
            try {
                $client   = new Client();
                $response = $client->request('POST', Self::BASE_RATEPLANS_URL, [
                    "headers" => [
                        "Content-type"  => "application/json",
                        "Authorization" => $this->fetch_token(),
                    ],
                    "body"    => $graphql,
                ]);
                $data = json_decode($response->getBody());

                if ( $msg = $this->is_error( $data ) ) {
                    trigger_error( $msg, E_USER_WARNING );
                    return $msg;
                }

                $roomtypeCached->store($queryHashed, $data, $this->roomtype_cache_timer);
            } catch (BadResponseException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                $data = [
                    'error' => $responseBodyAsString,
                    'data' => $response,
                ];
            }
        }

        return $data;
    }
}