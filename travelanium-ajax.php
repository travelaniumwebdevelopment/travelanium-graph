<?php

use Travelanium\TravelaniumGraph;
require 'travelanium.php';

/**
 * Method json_response
 *
 * @param Boolean $success 
 * @param Object $data
 * @param Int $http_code
 *
 * @return void
 */
function tl_json_response( $success, $data, $http_code = NULL, $response_to_root = [] ) {
    if (!$http_code) {
        $http_code = 200;

        if (!$success) {
            $http_code = 400;
        }
    }

    header('Content-type: application/json');
    http_response_code( $http_code );

    $data = array_merge( [
        'success' => $success,
        'data' => $data,
        'http_response_code' => $http_code,
    ], $response_to_root );

    echo json_encode( $data );

    die();
}

/**
 * Test HTTP Origin
 */
$origin = $_SERVER['HTTP_ORIGIN'];

if ( CORS_DOMAIN_ALLOWED && in_array( $origin, ALLOWED_DOMAINS ) ) {
    header( "Access-Control-Allow-Origin: $origin" );
} else {
    tl_json_response( false, [
        'message' => 'Blocked by CORS policy'
    ], 401 );
}

/**
 * Test ACTION parameter
 */
$action = $_POST['action'];

if ( !isset($action) ) {
    tl_json_response( false, [
        'message' => 'No param: action set',
    ], 400 );
}

/**
 * Test ACTION Allowed
 */
$allowed_actions = [
    'get_rateplans',
    'get_rateplan',
    'get_roomtypes',
    'get_roomtype,'
];

if ( !in_array($action, $allowed_actions) ) {
    tl_json_response( false, [
        'message' => "Action $action not allowed",
    ], 400 );
}

/**
 * Create TravelaniumGraph instance
 */
$travelanium = new TravelaniumGraph( [
    'api_key' => TRAVELANIUM_API_KEY,
] );

if ($action === 'get_rateplans') {
    $params = $_POST['params'];
    $rates = (array) $travelanium->get_rateplans( $params );

    tl_json_response( true, [], NULL, $rates );
}

if ($action === 'get_rateplan') {
    $rateId = $_POST['rateId'];

    if (!isset($rateId)) {
        tl_json_response(false, [
            'message' => 'param: rateId required'
        ], 400);
    }

    $params = $_POST['params'];
    $rate   = (array) $travelanium->get_rateplan( $rateId, $params );

    tl_json_response( true, [], NULL, $rate );
}

if ($action === 'get_roomtypes') {
    $params     = $_POST['params'];
    $roomtypes  = (array) $travelanium->get_roomtypes( $params );

    tl_json_response( true, [], NULL, $roomtypes );
}

if ($action === 'get_roomtype') {
    $roomId = $_POST['roomId'];

    if (!isset($roomId)) {
        tl_json_response(false, [
            'message' => 'param: roomId required'
        ], 400);
    }

    $params     = $_POST['params'];
    $roomtype   = (array) $travelanium->get_roomtype( $roomId, $params );

    tl_json_response( true, [], NULL, $roomtype );
}