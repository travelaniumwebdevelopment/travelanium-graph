<?php


/**
 * TRAVELANIUM_API_KEY
 * 
 * สามารถขอได้จาก Travelanium หากไม่มี API Key จะทำให้ไม่สามารถ Request Token ที่จำเป็นสำหรับการเรียกข้อมูลต่าง ๆ ผ่าน GraphQL ของ Travelanium ได้
 * 
 * @var STRING
 */
define('TRAVELANIUM_API_KEY', '__your_api_key_here__');

/**
 * TRAVELANIUM_CACHE_PATH
 * 
 * เซ็ตตำแหน่งเซฟไฟล์ Cache
 * โดย directory นี้จะต้องได้สิทธิ์อนุญาตให้ system สามารถ read/write/excute (แนะนำ: 0755) ได้
 * 
 * @var STRING
 */
define('TRAVELANIUM_CACHE_PATH', __DIR__ . '/cache/');

/**
 * TOKEN_REFRESH_TIMER
 * 
 * ระบบจะทำการรีเฟรช token เมื่อมีอายุตามเวลาที่กำหนก
 * 
 * default: 43200 (in seconds)
 * 
 * @var INT
 */
define('TOKEN_REFRESH_TIMER', 43200);

/**
 * QUERY_CACHE_TIMER
 * 
 * คิวรี่ทั้งหมดจะมีการแคชเอาไว้ตามกำหนด เพื่อลด Traffic
 * 
 * default: 600 (in seconds)
 * 
 * @var INT
 */
define('QUERY_CACHE_TIMER', 600);

/**
 * CORS_DOMAIN_ALLOWED
 * 
 * สำหรับการตั้งค่าการอนุญาตให้สามารถเรียกข้อมูลจากระยะไกลได้หรือไม่ได้
 * 
 * default: true
 * 
 * @var BOOLEAN
 */
define('CORS_DOMAIN_ALLOWED', true);

/**
 * ALLOWED_DOMAINS
 * 
 * รายการโดเมนที่ได้รับอนุญาตให้เรียกข้อมูลจากระยะไกลได้
 * โดย CORS_DOMAIN_ALLOWED จำเป็นต้องตั้งค่าเป็น true
 * 
 * @var ARRAY
 */
define('ALLOWED_DOMAINS', [
  'http://localhost:8000',
  'https://localhost:8000',
] );